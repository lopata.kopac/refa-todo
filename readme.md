# This is small example for presentasion

Core technologies in project: Typescript, React, Vite, Styled-Compoment, Axios, ReactQuery, Eslint...

## Run Project

Project use LF end of line. For remove errors with end of line you can run command for eslint.

```text
    npm run lint
```

### Backend

Backed app id in folder `backend`, run `npm start` fron run project. Follow comand to run app on first clone project:

``` text
    cd backend
    npm install
    npm start
```

You can change port in `index.js`.

### Frontend

Backed app id in folder `fronted`, run `npm start` fron run project. Follow comand to run app on first clone project:

``` text
    cd frontend
    npm install
    npm start
```

You can change port in `vite.config.ts`. For settign backend path check `.env` file.

```javascript
server: {
    port: 3000,
}

```

## VSCode

### Eslint

If you are using VSCode editor you can add a setting for eslint format after save the file. It would help if you had to install eslint Extension. For add on change the auto format setting to VSCode press `Ctrl + + Shift + P` or `Command + Shift + P` pick `Preferences: Open Workspace Settings(JSON)` and add this code to `.vscode/settings.json` file.

```javasript
{
    "eslint.validate": [
        "javascript",
        "javascriptreact",
        "typescript",
        "typescriptreact"
      ],
      "editor.codeActionsOnSave": {
        "source.fixAll.eslint": true,
      },
}

```

### Debugging

For debugging setting to VSCode press `Ctrl + + Shift + D` or `Command + Shift + D` select `create a launch.json` and add this code to `.vscode/launch.json` file. You need run project before debuging.

```javascript
{
    "version": "0.2.0",
    "configurations": [
        {
            "type": "chrome",
            "request": "launch",
            "name": "Launch Chrome against localhost",
            "url": "http://localhost:3000",
            "webRoot": "${workspaceFolder}"
        }
    ]
}

```
