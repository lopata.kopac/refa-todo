const express = require('express')
const cors = require('cors')

const app = express()
const port = 3001

app.use(cors())

const todos = [
  {
    id: "1",
    title: "Go shopping",
  },
  {
    id: "2",
    title: "Job interview",
  },
  {
    id: "3",
    title: "Prepare homework",
  },
]

app.get('/api/todo', (req, res) => {
  res.send(todos)
})

app.get('/api/todo/:id', (req, res) => {
  res.send(todos.find(todo => todo.id === req.params.id))
})

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})