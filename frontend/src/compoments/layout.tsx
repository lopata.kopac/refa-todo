import styled from "styled-components";

type Props = {
  children?: React.ReactNode | React.ReactNode[];
  classes?: string;
}

const StyledLayout = styled.div`
    background-color: #242424;
    
    min-height: 100vh;
    margin: 0;

    display: flex;
    justify-content: center;
    align-items: center;

    flex-direction: column;
`;

function Layout({ children, classes = '' }: Props) {
  return (
    <StyledLayout className={'layout' + classes} >
      {children}
    </StyledLayout>
  );
}

export default Layout;