import styled from 'styled-components';

type Props = {
  children?: React.ReactNode | React.ReactNode[];
  onClick?: () => void;
  tittle?: string;
  classes?: string;
}

const StyledSmallCard = styled.div`
    min-width: 160px;
    border-radius: 5px;
    margin: 10px;
    
    cursor: ${props => props.onClick ? 'pointer' : 'default'};
    
    background-color: #fff;
    color: #000;
    box-shadow: 5px 5px 5px rgba(68, 68, 68, 0.6);

    :hover {
      background-color:  ${props => props.onClick ? '#D3D3D3' : '#fff'};
    }

    label {
        font-weight: bold;
    }

    .small-card-tittle {
        font-weight: bold;
        font-size: 1.2em;

        border-bottom: 1px solid #eee;
        padding: 10px;
        margin-bottom: 5px;

        box-shadow: 0 1px 1px rgba(68, 68, 68, 0.6);
    }

    .small-card-contect {
        padding: 10px;
    }
`;

function SmallCard({ children, onClick, tittle, classes = ''}: Props) {
  return (
    <StyledSmallCard className={'small-card' + classes} onClick={onClick}>
      {tittle && <div className='small-card-tittle'>{tittle}</div>}
      <div className='small-card-contect'>{children}</div>
    </StyledSmallCard>
  );
}

export default SmallCard;