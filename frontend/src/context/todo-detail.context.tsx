import { createContext, ReactNode, useContext, useState } from "react";
import { useQuery } from "react-query";

import { fetchTodosById } from "../calls/todos";
import { Todo } from "../models/todo";

type Props = {
  children: ReactNode;
  id: string;
};

/**
 * This context is used to store the todo detail data.
 */
export const TodoDetailContext = createContext<{
  data?: Todo,
  isLoading: boolean,
  error?: any,
  setId: (id: string) => void}>
    ({
      isLoading: true,
      setId: () => {}
    });

/**
 * This context provider is used to store the todo detail data.
 */
export function TodoDetailContextProvider({ children, id }: Props) {
  const [todoId, setTodoId] = useState(id);
  const { data, isLoading, error } = useQuery(['todo', todoId], () => fetchTodosById(todoId));

  return (
    <TodoDetailContext.Provider value={{
      data: data?.data,
      isLoading,
      error,
      setId: setTodoId
    }}>{children}</TodoDetailContext.Provider>
  );
}

/**
 * This hook is used to get the todo detail data from the context.
 */
export function useTodoDetailContext() {
  const context = useContext(TodoDetailContext);
  if (context == null) {
    throw new Error("useTodoDetail must be used within a TodoDetailProvider");
  }
  return context;
}