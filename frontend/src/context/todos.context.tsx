import { createContext, ReactNode, useContext } from "react";
import { useQuery } from "react-query";

import { fetchTodos } from "../calls/todos";
import { Todo } from "../models/todo";

type Props = {
  children: ReactNode;
};

/**
 * This context is used to store the todo list data.
 */
export const TodosContext = createContext<{data: Todo[], isLoading: boolean, error?: any}>({data: [], isLoading: false});

/**
 * This context provider is used to store the todo list data.
 */
export function TodosContextProvider({ children }: Props) {
  const { data, isLoading, error } = useQuery('todos', fetchTodos);

  return (
    <TodosContext.Provider value={{
      data: data?.data || [],
      isLoading,
      error
    }}>{children}</TodosContext.Provider>
  );
}

/**
 * This hook is used to get the todo list data from the context.
 */
export function useTodosContext() {
  const context = useContext(TodosContext);
  if (context == null) {
    throw new Error("useTodos must be used within a TodosProvider");
  }
  return context;
}
