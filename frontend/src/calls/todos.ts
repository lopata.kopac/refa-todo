import axios from 'axios';

import { Todo } from '../models/todo';

/**
 * Calls the API to get all todos.
 * @returns {Promise<Todo[]>} The lits of todos.
 * @example
 * getAllTodos()
 *   .then((todos) => {
 *     console.log(todos);
 *   })
 *   .catch((error) => {
 *     console.log(error);
 *   });
 */
export function fetchTodos() {
  const { VITE_BACKEND_API_PATH } = import.meta.env;
  return axios.get<Todo[]>(`${VITE_BACKEND_API_PATH}/todo`);
}

/**
 * Calls the API to get todo by id.
 * @param {number} id - The id of the todo.
 * @returns {Promise<Todo>} - The single todo.
 * @example
 * getTodoById(1)
 *   .then((todo) => {
 *     console.log(todo);
 *   })
 *   .catch((error) => {
 *     console.log(error);
 *   });
 */
export function fetchTodosById(id: number | string) {
  const { VITE_BACKEND_API_PATH } = import.meta.env;
  return axios.get<Todo>(`${VITE_BACKEND_API_PATH}/todo/${id}`);
}