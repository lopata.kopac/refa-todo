import { memo } from "react";

import TodoCard from "./compomets/todo-card";
import { useTodosContext } from "../../context/todos.context";
import { Todo } from "../../models/todo";

const MemorizedTodoList = memo(function TodoList({todos}: {todos: Todo[]}) {
  return (
    <>
      {todos.map((todo) => (
        <TodoCard key={todo.id} todo={todo} />
      ))}
    </>
  );
});

function Todos() {
  const { data: todos, isLoading } = useTodosContext();
  
  if(isLoading) {
    return <div>...</div>;
  }

  return <MemorizedTodoList todos={todos} />;
}

export default Todos;