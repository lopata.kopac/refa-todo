import Todos from './todos';
import { TodosContextProvider } from '../../context/todos.context';

function TodoPage() {
  return (
    <TodosContextProvider >
      <Todos />
    </TodosContextProvider>
  );
}

export default TodoPage;