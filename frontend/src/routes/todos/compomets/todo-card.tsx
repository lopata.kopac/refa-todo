import { useNavigate } from "react-router-dom";

import SmallCard from "../../../compoments/small-card";
import { Todo } from "../../../models/todo";

type Props = {
  todo: Todo;
}

function TodoCard({ todo }: Props) {
  const navigate = useNavigate();

  function handleClick(id: number) {
    navigate("/detail/" + id);
  }

  return(
    <SmallCard onClick={()=>handleClick(todo.id)}>
      {todo.title}
    </SmallCard>
  );
}

export default TodoCard;