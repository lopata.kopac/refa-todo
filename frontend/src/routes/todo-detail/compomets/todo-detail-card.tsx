import SmallCard from "../../../compoments/small-card";
import { Todo } from "../../../models/todo";

type Props = {
  todo: Todo;
}

function TodoDetailCard({todo}: Props) {
  return(
    <SmallCard tittle='Todo'>
      <div><label>Id:&nbsp;</label>{todo.id}</div>
      <div><label>Tittle:&nbsp;</label>{todo.title}</div>
    </SmallCard>
  );
}

export default TodoDetailCard;