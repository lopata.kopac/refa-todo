import { useParams } from "react-router-dom";

import TodoDetail from "./todo-detail";
import { TodoDetailContextProvider } from "../../context/todo-detail.context";

function TodoDetailPage() {
  const { id } = useParams();

  return (
    <TodoDetailContextProvider id={id || '1'}>
      <TodoDetail />
    </TodoDetailContextProvider>
  );
}

export default TodoDetailPage;