import TodoDetailCard from "./compomets/todo-detail-card";
import { useTodoDetailContext } from "../../context/todo-detail.context";

function Todos() {
  const { data: todo, isLoading  } = useTodoDetailContext();

  if(isLoading){
    return <div>...</div>;
  }

  return (<>{todo && <TodoDetailCard todo={todo}/>}</>);
}

export default Todos;