import { Suspense } from 'react';
import { Route, Routes } from 'react-router-dom';

import TodosPage from './routes/todos';
import TodoDetailPage from './routes/todo-detail';
import Layout from './compoments/layout';

function App() {
  return (
    <Layout>
      <Routes>
        <Route
          index
          element={
            <Suspense fallback={<>...</>}>
              <TodosPage/>
            </Suspense>
          }
        />
        <Route
          path='detail/:id'
          element={
            <Suspense fallback={<>...</>}>
              <TodoDetailPage/>
            </Suspense>
          }
        />
      </Routes>
    </Layout>
  );
}

export default App;
